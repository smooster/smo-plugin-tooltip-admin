class SmoosterPluginToolTip
  smooster_extra: new Object
  all_tooltips: []

  constructor: () ->
    #empty, use if needed

  loadAllTooltips: ->
    $('[data-smo-plugin-tooltip] .tooltip').each (index, tooltip_container) =>
      tooltip_container = $(tooltip_container)
      @loadToolTip(tooltip_container)

  loadToolTip: (element) ->
    element = $(element) unless element instanceof jQuery

    return if element.find(".tooltiptext").length > 0

    tooltip_body = $('<span class="tooltiptext" style="display: none;"><span class="tooltipbump"></span><span class="actualtext">Dies ist ein Platzhaltertext!</span><span class="close-button" style="display: none;"></span></span><img src="http://assets.smooster.me/55e57df862273d609f0002a4/img/tooltip.svg" alt="tooltip">')

    element.append(tooltip_body)

  observeNewTooltips: ->
    plugin_helper = @
    # zu überwachende Zielnode (target) auswählen
    target = document.querySelector('[data-smo-plugin-tooltip]')
    # eine Instanz des Observers erzeugen
    observer = new MutationObserver((mutations) ->
      mutations.forEach (mutation) ->
        if $(mutation.addedNodes).find('.tooltip').length > 0
          $(mutation.addedNodes).find('.tooltip').each (index, tooltip) ->
            plugin_helper.loadToolTip(tooltip)
            plugin_helper.create_toolbar(tooltip)
        return
      return
    )
    config =
      attributes: false
      childList: true
      characterData: false
    # eigentliche Observierung starten und Zielnode und Konfiguration übergeben
    observer.observe target, config
    # später: Observation wieder einstellen
    #observer.disconnect()

  create_toolbar: (target) ->
    $(target).each (index, single_target) =>
      btn_edit = @ui_helper.create_button("edit","fa-edit")
      btn_edit.click (e) =>
        e.preventDefault()
        $(single_target).find('.tooltiptext').show()
        @ui_helper.create_inplace_editor($(single_target).find('.actualtext'))

      @ui_helper.create_toolbar(single_target,[btn_edit])

  destroy_toolbars: ->
    @ui_helper.destroy_gui()

  load_ui: () ->
    @smooster_extra.debugMsg "loading UI..."

    #@append_style()

    @ui_helper = @smooster_extra.ui_helper

    @create_toolbar($('[data-smo-plugin-tooltip] .tooltip'))

    # alle tooltips in der Seite vorbereiten
    @loadAllTooltips()

    @observeNewTooltips()

    # Nach Änderungen HTML in Slideshow überschreiben und glide neu starten
    # Vor dem Speichern Glide löschen
    @smooster_extra.editor.on "before_save", =>
      @destroy_toolbars()

    @smooster_extra.editor.on "after_save", =>
      @create_toolbar($('[data-smo-plugin-tooltip] .tooltip'))

    @smooster_extra.editor.on "editor_change", =>
      @destroy_toolbars()
      @create_toolbar($('[data-smo-plugin-tooltip] .tooltip'))

  append_style: () ->
    $("<style type='text/css'> .smo-glide-adminarea .slide{ float: none; border-bottom: 1px solid grey; margin-bottom: 30px;} </style>").appendTo("head");

  @init: ->
    SmoosterExtra.instance.debugMsg "loading ToolTipPlugin"
    return false unless SmoosterExtra.instance.api_version >= 1
    plugin_instance = new SmoosterPluginToolTip
    plugin_instance.smooster_extra = SmoosterExtra.instance
    plugin_instance.config = window.SmoosterExtraConfig.config.tooltip

    plugin_instance.load_ui()
    #call your internal PluginMethod to start with what ever fancy your plugin does.

@SmoosterPluginToolTip or (@SmoosterPluginToolTip = SmoosterPluginToolTip)
