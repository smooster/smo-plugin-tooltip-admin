@SmoosterExtra or (@SmoosterExtra = {})
jQuery.extend @SmoosterExtra,
  config:
      tooltip:
            arrowRightText: '<i class="fa fa-play"></i>'
            arrowLeftText: '<i class="fa fa-play fa-rotate-180"></i>'
            navigation: '.slider_navigation'
            navigationCenter: false
            navigationClass: 'th_slide_navi'
            navigationItemClass: "th_slide_navi_item"
            navigationCurrentItemClass: "th_slide_navi_item_current"
      after_load: ->
        console.log "after load callback"

  debug: false
